# Use an existing lightweight web server image as the base image
FROM nginx:alpine

# Copy your HTML file into the appropriate directory in the container
COPY index.html /usr/share/nginx/html/
